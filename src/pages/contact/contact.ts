import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  posts: any;

  constructor(public navCtrl: NavController, public http: Http) {

  }

  ngOnInit() {
    this.http
      .get(
        'http://api.openweathermap.org/data/2.5/weather?id=3109498&units=metric&appid=1e21fb83e9ed74c9331768638405d612'
      )
      .map(res => res.json())
      .subscribe(data => {
        this.posts = [data];
      });
  }
}
